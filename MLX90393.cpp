//
// MLX90393.cpp : arduino driver for MLX90393 magnetometer
//
// Copyright 2016 Theodore C. Yapo
//
// released under MIT License (see file)
//

#include "MLX90393.h"


uint8_t
MLX90393::
begin(uint8_t res, int CS_pin, SPISettings spi_settings, int DRDY_pin, SPIClass &SPIPort)
{
  _spiPort = &SPIPort;
  this->CS_pin = CS_pin;

  this->DRDY_pin = DRDY_pin;
  if (DRDY_pin >= 0){
    pinMode(DRDY_pin, INPUT);
  }


  //设置CS脚
  pinMode(this->CS_pin, OUTPUT);
  cs(HIGH);

  _spiPort->begin();

  //设置SPI工作模式
  _spiPort->beginTransaction(spi_settings);

  uint8_t status1 = checkStatus(reset());
  uint8_t status2 = setGainSel(7);
  uint8_t status3 = setResolution(res, res, res);
  uint8_t status4 = setOverSampling(0);
  uint8_t status5 = setDigitalFiltering(0);
  uint8_t status6 = setTemperatureCompensation(0);
  uint8_t status7 = setHallConf(0);

  return status1 | status2 | status3 | status4 | status5 | status6;
}

uint8_t
MLX90393::
fillReg()
{
  uint8_t i;
  uint8_t status = 0;
  for (i=0;i<3;i++) {
    status |= readRegister(i, reg[i]);
  }
  return status;
}

uint8_t
MLX90393::
startBurst(uint8_t zyxt_flags)
{
  fillReg();
  return sendCommand(CMD_START_BURST | (zyxt_flags & 0xf));
}

uint8_t
MLX90393::
startMeasurement(uint8_t zyxt_flags)
{
  //读取寄存器
  fillReg();
  return sendCommand(CMD_START_MEASUREMENT | (zyxt_flags & 0xf));
}

uint8_t
MLX90393::
readMeasurement(uint8_t zyxt_flags, txyzRaw& txyz_result)
{
  uint8_t status;
  uint8_t buffer[8];

  uint8_t count = (((zyxt_flags & Z_FLAG)?2:0) +
                   ((zyxt_flags & Y_FLAG)?2:0) +
                   ((zyxt_flags & X_FLAG)?2:0) +
                   ((zyxt_flags & T_FLAG)?2:0) );

  cs(LOW);
  _spiPort->transfer(CMD_READ_MEASUREMENT | (zyxt_flags & 0xf));
  status = _spiPort->transfer(0);
  if (status==STATUS_ERROR) {
    cs(HIGH);
    return STATUS_ERROR;
  }
  for (uint8_t i=0; i < count; i++){
    buffer[i] = _spiPort->transfer(0);
  }
  cs(HIGH);

  uint8_t i = 0;
  if (zyxt_flags & T_FLAG){
    txyz_result.t =  (uint16_t(buffer[i])<<8) | buffer[i+1];
    i += 2;
  } else {
    txyz_result.t = 0;
  }
  if (zyxt_flags & X_FLAG){
    txyz_result.x =  (uint16_t(buffer[i])<<8) | buffer[i+1];
    i += 2;
  } else {
    txyz_result.x = 0;
  }
  if (zyxt_flags & Y_FLAG){
    txyz_result.y =  (uint16_t(buffer[i])<<8) | buffer[i+1];
    i += 2;
  } else {
    txyz_result.y = 0;
  }
  if (zyxt_flags & Z_FLAG){
    txyz_result.z =  (uint16_t(buffer[i])<<8) | buffer[i+1];
    i += 2;
  } else {
    txyz_result.z = 0;
  }

  return status;
}


uint8_t
MLX90393::
readDataRaw(MLX90393::txyzRaw& data)
{
  uint8_t status1 = startMeasurement(X_FLAG | Y_FLAG | Z_FLAG | T_FLAG);

  // wait for DRDY signal if connected, otherwise delay appropriately
  if (DRDY_pin >= 0){
    delayMicroseconds(600);
    while(!digitalRead(DRDY_pin)){
      // busy wait
    }
  } else {
    delay(this->convDelayMillis());
  }

  txyzRaw raw_txyz;
  uint8_t status2 = readMeasurement(X_FLAG | Y_FLAG | Z_FLAG | T_FLAG, raw_txyz);
  data = raw_txyz;
  return checkStatus(status1) | checkStatus(status2);
}

uint16_t
MLX90393::
convDelayMillis() {
  const uint8_t osr = (reg[OSR_REG] & OSR_MASK) >> OSR_SHIFT;
  const uint8_t osr2 = (reg[OSR2_REG] & OSR2_MASK) >> OSR2_SHIFT;
  const uint8_t dig_flt = (reg[DIG_FLT_REG] & DIG_FLT_MASK) >> DIG_FLT_SHIFT;

  return
    (DRDY_pin >= 0)? 0 /* no delay if drdy pin present */ :
                     // estimate conversion time from datasheet equations
                     ( 3 * (2 + (1 << dig_flt)) * (1 << osr) *0.064f +
                      (1 << osr2) * 0.192f ) *
                       1.3f;  // 30% tolerance
}

uint8_t
MLX90393::
setHallConf(uint8_t hallconf)
{
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(HALLCONF_REG, old_val);
  new_val = (old_val & ~HALLCONF_MASK) | ((uint16_t(hallconf) << HALLCONF_SHIFT) & HALLCONF_MASK);
  status2 = writeRegister(HALLCONF_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
setDigitalFiltering(uint8_t dig_flt)
{
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(DIG_FLT_REG, old_val);
  new_val = (old_val & ~DIG_FLT_MASK) | ((uint16_t(dig_flt) << DIG_FLT_SHIFT) & DIG_FLT_MASK);
  status2 = writeRegister(DIG_FLT_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
setTemperatureCompensation(uint8_t enabled)
{
  uint8_t tcmp_en = enabled?1:0;
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(TCMP_EN_REG, old_val);
  new_val = (old_val & ~TCMP_EN_MASK) | ((uint16_t(tcmp_en) << TCMP_EN_SHIFT) & TCMP_EN_MASK);
  status2 = writeRegister(TCMP_EN_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
setOverSampling(uint8_t osr)
{
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(OSR_REG, old_val);
  new_val = (old_val & ~OSR_MASK) | ((uint16_t(osr) << OSR_SHIFT) & OSR_MASK);
  status2 = writeRegister(OSR_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
setResolution(uint8_t res_x, uint8_t res_y, uint8_t res_z)
{
  uint16_t res_xyz = ((res_z & 0x3)<<4) | ((res_y & 0x3)<<2) | (res_x & 0x3);
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(RES_XYZ_REG, old_val);
  new_val = (old_val & ~RES_XYZ_MASK) | ((res_xyz << RES_XYZ_SHIFT) & RES_XYZ_MASK);
  status2 = writeRegister(RES_XYZ_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
setGainSel(uint8_t gain_sel)
{
  uint16_t old_val, new_val;
  uint8_t status1, status2;
  status1 = readRegister(GAIN_SEL_REG, old_val);
  new_val = (old_val & ~GAIN_SEL_MASK) | ((uint16_t(gain_sel) << GAIN_SEL_SHIFT) & GAIN_SEL_MASK);
  status2 = writeRegister(GAIN_SEL_REG, new_val);
  return checkStatus(status1) | checkStatus(status2);
}

uint8_t
MLX90393::
checkStatus(uint8_t status)
{
  return (status & ERROR_BIT) ? STATUS_ERROR : STATUS_OK;
}

uint8_t
MLX90393::
reset()
{
  uint8_t status;
  status = sendCommand(CMD_RESET);
  delay(2);
  return status;
}

void
MLX90393::
cs(uint8_t val)
{
  digitalWrite(CS_pin, val);
}

uint8_t
MLX90393::
readRegister(uint8_t address, uint16_t& data)
{
  uint8_t status;
  cs(LOW);
  _spiPort->transfer(CMD_READ_REGISTER);
  _spiPort->transfer(address<<2);
  status = _spiPort->transfer(0);
  data = uint16_t(_spiPort->transfer(0))<<8;
  data |= _spiPort->transfer(0);
  cs(HIGH);
  return status;
}

uint8_t
MLX90393::
writeRegister(uint8_t address, uint16_t data)
{
  uint8_t status;
  cs(LOW);
  _spiPort->transfer(CMD_WRITE_REGISTER);
  _spiPort->transfer(uint8_t(data >> 8));
  _spiPort->transfer(uint8_t(data & 0xFF));
  _spiPort->transfer(address<<2);
  status = _spiPort->transfer(0);
  cs(HIGH);
  return status;
}

uint8_t
MLX90393::
sendCommand(uint8_t cmd)
{
  uint8_t status;
  cs(LOW);
  _spiPort->transfer(cmd);
  status = _spiPort->transfer(0);
  cs(HIGH);
  return status;
}
