/*
    SPI Master Demo Sketch
    Connect the SPI Master device to the following pins on the esp8266:

    GPIO    NodeMCU   Name  |   Uno
   ===================================
     15       D8       SS   |   D10
     13       D7      MOSI  |   D11
     12       D6      MISO  |   D12
     14       D5      SCK   |   D13

    Note: If the ESP is booting at a moment when the SPI Master has the Select line HIGH (deselected)
    the ESP8266 WILL FAIL to boot!
    See SPISlave_SafeMaster example for possible workaround

*/
#include <SPI.h>
#include "MLX90393.h"

#define RESOLUTION 3
#define DRDY_P 5

MLX90393 mlx;
int cc = 0;

#define LIST_SIZE 50
static MLX90393::txyzRaw datalist[LIST_SIZE];
static int data_pos = 0;
static int data_count = 0;
MLX90393::txyzRaw dataRaw;
uint16_t Tref;
uint16_t Sens;


// void init_mlx90393() {

//   // uint8_t status1 = checkStatus(reset());
//   // uint8_t status2 = setGainSel(7);
//   // uint8_t status3 = setResolution(res, res, res);
//   // uint8_t status4 = setOverSampling(3);
//   // uint8_t status5 = setDigitalFiltering(7);
//   // uint8_t status6 = setTemperatureCompensation(0);  

//   uint8_t status;

//   //Reset
//   digitalWrite(SS, LOW);
//   status = SPI.transfer(0xF0);
//   status = SPI.transfer(0);
//   digitalWrite(SS, HIGH);
//   Serial.print("Reset, status=");
//   Serial.println(status);
//   delay(2);



//   Serial.println("Init mlx90393 successfully!");

// }

void setup() {

  //串口初始化，输出数据
  Serial.begin(115200);
  delay(5000);

  // //SPI初始化
  // SPI.begin();

  // //设置CS脚
  // pinMode(SS, OUTPUT);
  // digitalWrite(SS, HIGH);

  // //设置SPI工作模式
  // SPI.beginTransaction(SPISettings(SPI_CLOCK_DIV8, MSBFIRST, SPI_MODE3));

  // delay(5000);

  // Serial.println("MLX90393 SPI TEST......");

  // //初始化mlx90393
  // init_mlx90393();

  // mlx.begin(RESOLUTION, SS, SPISettings(SPI_CLOCK_DIV8, MSBFIRST, SPI_MODE3), SPI);
  while(mlx.begin(RESOLUTION, SS, SPISettings(SPI_CLOCK_DIV8, MSBFIRST, SPI_MODE3), DRDY_P, SPI) != MLX90393::STATUS_OK) {
  	Serial.print('.');
    delay(500);
  }

  uint8_t r1,r2,r3,r4,r5;
  // uint16_t Tref=0;
  r1=mlx.readRegister(0x24, Tref);
  uint16_t Sens_tc = 0;
  r2=mlx.readRegister(0x03, Sens_tc);
  Sens = Sens_tc & 0x7F;
  uint16_t offset_x=0, offset_y=0, offset_z=0;
  r3=mlx.readRegister(0x04, offset_x);
  r4=mlx.readRegister(0x05, offset_y);
  r5=mlx.readRegister(0x06, offset_z);

  //burst模式设置
  mlx.startBurst(0x0F);

}

// 直接通过spi读取数据
void loop_1() {
  uint8_t sm_status, rm_status;
  uint16_t xMag,yMag,zMag,tMag;
  xMag=yMag=zMag=tMag=0;

  cc++;
  Serial.print("cc=");
  Serial.print(cc);

  //发送SM(Start Single Measurement Mode) 命令
  digitalWrite(SS, LOW);
  sm_status = SPI.transfer(0x3F);
  sm_status = SPI.transfer(0);
  digitalWrite(SS, HIGH);
  Serial.print(", sm_status=");
  Serial.print(sm_status);

  digitalWrite(SS, LOW);
  rm_status = SPI.transfer(0x4F);
  rm_status = SPI.transfer(0);
  tMag |= SPI.transfer(0)<<8;
  tMag |= SPI.transfer(0) ;
  xMag |= SPI.transfer(0)<<8;
  xMag |= SPI.transfer(0) ;
  yMag |= SPI.transfer(0)<<8;
  yMag |= SPI.transfer(0) ;
  zMag |= SPI.transfer(0)<<8;
  zMag |= SPI.transfer(0) ;
  digitalWrite(SS, HIGH);

  Serial.print(", rm_status=");
  Serial.print(rm_status);

  Serial.print(", t=");
  Serial.print(tMag);
  Serial.print(", x=");
  Serial.print(xMag);
  Serial.print(", y=");
  Serial.print(yMag);
  Serial.print(", z=");
  Serial.print(zMag);

  Serial.println();

  delay(500);
}

//通过封装函数读取数据
void loop_2() {
  mlx.readDataRaw(dataRaw);

  datalist[data_pos] = dataRaw;
  data_pos++;
  if (data_pos >= LIST_SIZE)
    data_pos = 0;
  if (data_count < LIST_SIZE)
    data_count += 1;

  uint32_t xs,ys,zs,ts,xa,ya,za,ta;

  xs=ys=zs=ts=0;
  for (int i=0;i<data_count;i++) {
    xs += datalist[i].x;
    ys += datalist[i].y;
    zs += datalist[i].z;
    ts += datalist[i].t;
  }

  xa = round(xs / data_count);
  ya = round(ys / data_count);
  za = round(zs / data_count);
  ta = round(ts / data_count);

  uint32_t xtc,ytc,ztc;
  // xtc = 32768 + (dataRaw.x ^ 0x8000) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.x ^ 0x8000) / 4096;
  // ytc = 32768 + (dataRaw.y ^ 0x8000) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.y ^ 0x8000) / 4096;
  // ztc = 32768 + (dataRaw.z ^ 0x8000) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.z ^ 0x8000) / 4096;
  xtc = (dataRaw.x ) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.x) / 4096;
  ytc = (dataRaw.y ) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.y) / 4096;
  ztc = (dataRaw.z ) + Sens * (dataRaw.t - Tref) / 128 * (dataRaw.z) / 4096;

  cc++;

  Serial.print("cc=");
  Serial.print(cc);

  Serial.print(" x=");
  Serial.print(xtc);
  Serial.print(" y=");
  Serial.print(ytc);
  Serial.print(" z=");
  Serial.print(ztc);
  Serial.print(" t=");
  Serial.print(dataRaw.t);

  Serial.print(" xraw=");
  Serial.print(dataRaw.x);
  Serial.print(" yraw=");
  Serial.print(dataRaw.y);
  Serial.print(" zraw=");
  Serial.print(dataRaw.z);
  Serial.print(" t=");
  Serial.print(dataRaw.t);

  Serial.print(" xa=");
  Serial.print(xa);
  Serial.print("  ya=");
  Serial.print(ya);
  Serial.print("  za=");
  Serial.print(za);
  Serial.print(" ta=");
  Serial.print(ta);
 
  Serial.print(" dxy=");
  Serial.print(sqrt(1.0 * xa * xa + 1.0 * ya * ya));

  Serial.println();

  // uint16_t Tref=0;
  // mlx.readRegister(0x24, Tref);
  // uint16_t Sens_tc = 0;
  // mlx.readRegister(0x03, Sens_tc);
  // uint16_t offset_x=0, offset_y=0, offset_z=0;
  // mlx.readRegister(0x04, offset_x);
  // mlx.readRegister(0x05, offset_y);
  // mlx.readRegister(0x06, offset_z);
  
  // Serial.print("Tref=");
  // Serial.print(Tref);
  // Serial.print(" Sens_tc_lt=");
  // Serial.print(Sens_tc & 0x7F);
  // Serial.print(" Sens_tc_ht=");
  // Serial.print((Sens_tc & 0x7F00) >> 8);

  // Serial.print(" offset_x=");
  // Serial.print(offset_x);
  // Serial.print(" offset_y=");
  // Serial.print(offset_y);
  // Serial.print(" offset_z=");
  // Serial.print(offset_z);
  
  // Serial.println("");

  delay(995);
}

//burst模式读取数据
void loop() {

  // wait for DRDY signal if connected, otherwise delay appropriately
  if (mlx.DRDY_pin >= 0){
    // delayMicroseconds(600);
    while(!digitalRead(mlx.DRDY_pin)){
      // busy wait
    }
  } else {
    delay(mlx.convDelayMillis());
  }

  uint8_t status2 = mlx.readMeasurement(0x0F, dataRaw);
  cc++;
  // Serial.print("cc=");
  // Serial.print(cc);

  // Serial.print(" xraw=");
  // Serial.print(dataRaw.x);
  // Serial.print(" yraw=");
  // Serial.print(dataRaw.y);
  // Serial.print(" zraw=");
  // Serial.print(dataRaw.z);
  // Serial.print(" t=");
  // Serial.print(dataRaw.t);
  // Serial.println();

  Serial.print(dataRaw.x);
  Serial.print(",");
  Serial.print(dataRaw.y);
  Serial.print(",");
  Serial.print(dataRaw.z);
  Serial.print(",");
  Serial.print(dataRaw.t);
  Serial.println();

  // delay(20000);
}